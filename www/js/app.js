// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

// cordova plugin add https://github.com/MobileChromeApps/cordova-plugin-chrome-apps-sockets-tcp.git

var tcpClient = null;
var socketId = null;
var server_ip = '';

function ab2str(buf) {
  return String.fromCharCode.apply(null, new Uint16Array(buf));
}
function str2ab(str) {
  var buf = new ArrayBuffer(str.length*2); // 2 bytes for each char
  var bufView = new Uint16Array(buf);
  for (var i=0, strLen=str.length; i < strLen; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
}

angular.module('starter', ['ionic', 'ngCordova'])
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
      tcpClient = chrome.sockets.tcp;
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})
.config(function($httpProvider) {
      $httpProvider.defaults.useXDomain = true;
      delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
)
.factory('pouch', function(){
  var localDB;

  var service = {
    initialize: initialize
  }

  return service;

  function initialize(db_name){
    localDB = new PouchDB(db_name, {
      adapter: 'websql',
      auto_compaction: true
    });
    return localDB;
  }

})
.controller('mainCtrl', function($scope, $http, $cordovaFile, pouch){
  $scope.newDoc = {};
  $scope.success = '';
  $scope.syncserver_ip = '192.168.1.12:3000';
  $scope.couchdb_server = 'jspblm:kingcouch13@couchdb.jspblm.com:5984';
  $scope.db_name = 'user_sync1';
  $scope.dumped_string = false;
  $scope.data_sent = '1234567891234567';
  $scope.status_log = '';
  $scope.server_ip = '192.168.1.12';
  var dumpedString_data = '';
  var pouchdb = pouch.initialize($scope.db_name);

  var MemoryStream = window.memorystream;

  $scope.dump_pouchdb = function(){
    var stream = new MemoryStream();

    stream.on('data', function(chunk) {
      dumpedString_data += chunk.toString();
    });

    pouchdb.dump(stream).then(function() {
      console.log('dumpedString === ' + dumpedString_data);
      $scope.$apply(function(){
        $scope.dumped_string = true;
        $scope.dumpedString_data = dumpedString_data
      })
    }).catch(function (err) {
      console.log('Error dump_pouchdb !!!', err);
    });
  }

  // ============== TCP Client >>> =============
  $scope.socket_create = function(){
    tcpClient.create({name: 'tcp_client'}, function(createInfo){
      socketId = createInfo.socketId;
      console.log('createInfo', createInfo);
      $scope.$apply(function(){
        $scope.status_log = 'create ' + createInfo;
      });
    });
  }

  $scope.set_ip = function(valor){
    console.log('valor', valor);
    server_ip = valor;
    console.log('set_ip', server_ip);
  }

  $scope.connect = function(){
    console.log('connecting', server_ip);
    tcpClient.connect(socketId, server_ip, 8000, function(connect_info){
      console.log('connect', connect_info);
      $scope.$apply(function(){
        $scope.status_log = 'connect ' + connect_info;
      });
    });
  }

  $scope.disconnect = function(){
    tcpClient.disconnect(socketId, function(info){
      console.log('disconnect', info);
    });
  }

  $scope.send = function(data_sent){
    // var data_sent = $scope.data_sent;
    console.log('data_sent', data_sent);
    data_sent_ab = str2ab(data_sent);
    tcpClient.send(socketId, data_sent_ab, function(sendInfo){
      console.log('send', sendInfo);
      $scope.$apply(function(){
        $scope.status_log = 'sent ' + sendInfo;
      });
    });
  }

  $scope.send_dump = function(){
    // dumpedString_data
    console.log('dumpedString_data', dumpedString_data);
    var dumpedString_data_bf = str2ab(dumpedString_data);
    tcpClient.send(socketId, dumpedString_data_bf, function(sendInfo){
      console.log('send', sendInfo);
      $scope.$apply(function(){
        $scope.status_log = 'sent ' + sendInfo;
      });
    });
  }

  $scope.close = function(){
    tcpClient.close(socketId, function(info){
      console.log('close', info)
    });
  }
  // ============== <<< TCP Client =============
  // $scope.upload_to_server = function(){
  //   console.log('upload_to_server');
  //   $http
  //     .post(
  //       'http://' + $scope.syncserver_ip + '/pouchdb_dump_file/',
  //       dumpedString_data
  //       ,{
  //         headers : {
  //           'Content-Type' :
  //           'application/x-www-form-urlencoded; charset=UTF-8'
  //         }
  //       }
  //     )
  //     .then(
  //       function(response){
  //         console.log("Upload_to_server Response !!!", response);
  //       },
  //       function(error){
  //         console.log("Upload_to_server Error !!!", error);
  //     });
  // }

  $scope.write_file = function(){
    console.log('write_file');
    $cordovaFile.writeFile(cordova.file.externalDataDirectory,
      $scope.db_name + "_dump.txt", dumpedString_data, true)
      .then(function(success){
        // file:///storage/emulated/0/Android/data/app-id/files/
        console.log('writeFile success', success);
        $scope.write_file_status = 'writed!';
        // $scope.$apply(function(){
        //   $scope.write_file_status = 'writed!';
        // });
      }, function(error){
        console.log('writeFile error', error);
      });
  }

  $scope.sync_local_pouchdb = function() {
    $scope.sync_msg = "syncing";
    var sync_process = pouchdb.sync('http://' + $scope.couchdb_server + '/' + $scope.db_name,
    	   {live: true,
    	   retry: true})
      	.on('change', function(info){
      		console.log('change', info);
      	})
      	.on('paused', function(){
      		console.log('paused, now cancel!');
          sync_process.cancel();
      	})
      	.on('active', function(){
      		console.log('active');
      	})
      	.on('denied', function(info){
      		console.log('denied', info);
      	})
      	.on('complete', function(info){
          // no se esta ejecutando
      		console.log('complete', info);
      	})
      	.on('error', function(err){
      		console.log('error', err);
      	});
  }

  $scope.createNewObject = function(){
      console.log('New Object === ', $scope.newDoc);
      $scope.newDoc.id = uuid.v4();
      pouchdb
        .put({
          _id: $scope.newDoc.id,
          name: $scope.newDoc.name,
          profession: $scope.newDoc.profession
        })
        .then(function(response){
          console.log("createNewObject response === ", response);
          $scope.$apply(function(){
            $scope.success = "Created OK!! ID: " + $scope.newDoc.id + "  User: " + $scope.newDoc.name;
            $scope.newDoc = {};
          });

          // setTimeout(function(){
          //   $scope.$apply(function(){
          //     $scope.success = '';
          //   });
          // }, 2000);
        });
    }

    $scope.see_all_docs = function(){
      pouchdb.allDocs({
        include_docs: true
      }).then(function(result){
        console.log("result.rows === ", result.rows);
        $scope.docs = result.rows;
      }).catch(function(err){
        console.log("Error allDocs === ", err);
      })
    }

});
